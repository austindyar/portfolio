#pragma once

/*
khronos/julian_calendar.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Julian calendar header.
*/

#include <khronos/calendar.hpp>
#include <khronos/julian_day.hpp>

namespace khronos {
	// VALUES
	// =============================================

	/**	JD of the start of the Julian epoch. */
	jd_t constexpr JULIAN_EPOCH = 1'721'423.5;

	// FUNCTIONS
	// =============================================
	inline bool is_julian_leapyear(year_t year) { return year % 4 == 0; }
	jd_t julian_to_jd(year_t year, month_t month, day_t day);
	jd_t julian_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds);
	void jd_to_julian(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second);
	void jd_to_julian(jd_t jd, year_t& year, month_t& month, day_t& day);

	/* UDL - converts a Julian year BCE to an astronomical Julian year. */
	constexpr year_t operator ""_BC(unsigned long long julianYearBC) { return - static_cast<year_t>(julianYearBC) + 1; }
	constexpr year_t operator ""_bc(unsigned long long julianYearBC) { return - static_cast<year_t>(julianYearBC)+ 1; }

	constexpr year_t operator ""_C(unsigned long long julianYearC) { return static_cast<year_t>(julianYearC); }
	constexpr year_t operator ""_c(unsigned long long julianYearc) { return static_cast<year_t>(julianYearc); }

	inline day_t julian_days_in_month(month_t month, bool isLeapYear) { return civil::days_in_month(month, isLeapYear); }

	inline char const* julian_month_name(month_t month) { return civil::month_name_long(month); }

	inline char const* julian_short_month_name(month_t month) { return civil::month_name_short(month); }

	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Proleptic Julian Calendar Date class. */
	class Julian {
		year_t	year_;
		month_t	month_;
		day_t day_;
		hour_t hour_;
		minute_t minute_;
		second_t second_;

		void from_jd(jd_t jd) { jd_to_julian(jd, year_, month_, day_,hour_,minute_,second_); }
		jd_t to_jd() const { return julian_to_jd(year_, month_, day_, hour_, minute_, second_); }
	public:
		Julian();

		year_t year() const { return year_; }
		month_t month() const { return month_; }
		day_t day() const { return day_; }
		hour_t hour() const { return hour_; }
		minute_t minute() const { return minute_; }
		second_t second() const { return second_; }

		Julian(year_t year, month_t month, day_t day) :year_(year), month_(month), day_(day), hour_(0), minute_(0), second_(0) {}
		Julian(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec);

		/** Construct a Julian date from Julian Day Number object. */
		Julian(Jd const& jd) { from_jd(jd.jd()); }

		/**	Implicit cast to Jd class. */
		operator Jd () const { return Jd(to_jd()); }

		/**	Assign and convert from Jd type to Julian type. */
		Julian& operator = (Jd const& jd) { from_jd(jd.jd()); return *this; }

		std::string Julian::to_string() const;
	// Block some operators
	private:
		Julian operator + (detail::packaged_year_real const&);
		Julian operator - (detail::packaged_year_real const&);
		Julian operator + (detail::packaged_month_real const&);
		Julian operator - (detail::packaged_month_real const&);
	};

	// OPERATORS
	// =============================================

	/** Julian + (integer month) */
	Julian operator + (Julian const& dt, detail::packaged_month_integer const& month);

	/** Julian - (integer month) */
	inline Julian operator - (Julian const& dt, detail::packaged_month_integer const& month) { return dt + detail::packaged_month_integer(-month.nMonths_); }

	/**	Julian + (integer year) */
	Julian operator + (Julian const& dt, detail::packaged_year_integer const& year);

	/**	Julian - (integer year) */
	inline Julian operator - (Julian const& dt, detail::packaged_year_integer const& year) { return dt + detail::packaged_year_integer(-year.nYears_); }

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Julian const& g) { return os << g.to_string(); }
}
