#pragma once

/*
khronos/gregorian_calendar.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'Gregorian calendar' declarations.
*/

#include <khronos/calendar.hpp>
#include <khronos/julian_day.hpp>
#include <cassert>

namespace khronos {
	// VALUES
	// =============================================

	/**	JD of the start of the Gregorian epoch. */
	jd_t constexpr GREGORIAN_EPOCH = 1'721'425.5;

	// FUNCTIONS
	// =============================================
	bool is_gregorian_leapyear(year_t year);
	jd_t gregorian_to_jd(year_t year, month_t month, day_t day);
	jd_t gregorian_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds);
	void jd_to_gregorian(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second);
	void jd_to_gregorian(jd_t jd, year_t& year, month_t& month, day_t& day);

	/* UDL - converts a Gregorian year CE to an astronomical Gregorian year. */
	constexpr year_t operator ""_CE(unsigned long long commonEra) { return static_cast<year_t>(commonEra); }
	constexpr year_t operator ""_ce(unsigned long long commonEra) { return static_cast<year_t>(commonEra); }

	/* UDL - converts a Gregorian year BCE to an astronomical Gregorian year. */
	constexpr year_t operator ""_BCE(unsigned long long beforeCommonEra) { return -static_cast<year_t>(beforeCommonEra) + 1; }
	constexpr year_t operator ""_bce(unsigned long long beforeCommonEra) { return -static_cast<year_t>(beforeCommonEra) + 1; }

	inline day_t gregorian_days_in_month(month_t month, bool isLeapYear) { return civil::days_in_month(month, isLeapYear); }

	inline char const* gregorian_month_name(month_t month) { return civil::month_name_long(month); }

	inline char const* gregorian_short_month_name(month_t month) { return civil::month_name_short(month); }


	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Proleptic Gregorian Calendar Date class. */
	class Gregorian {
		year_t	year_;
		month_t	month_;
		day_t day_;
		hour_t hour_;
		minute_t minute_;
		second_t second_;

		/** From and to JD. */
		void from_jd(jd_t jd) { jd_to_gregorian(jd, year_, month_, day_, hour_, minute_, second_); }
		jd_t to_jd() const { return gregorian_to_jd(year_, month_, day_, hour_, minute_, second_); }
	public:
		Gregorian();

		year_t year() const { return year_; }
		month_t month() const { return month_; }
		day_t day() const { return day_; }
		hour_t hour() const { return hour_; }
		minute_t minute() const { return minute_; }
		second_t second() const { return second_; }

		/** Constructors. */
		Gregorian(year_t year, month_t month, day_t day) : year_(year), month_(month), day_(day), hour_(0), minute_(0), second_(0) {}
		Gregorian(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec);

		/** Construct a Gregorian date from Julian Day Number object. */
		Gregorian(Jd const& jd) { from_jd(jd.jd()); }

		/**	Implicit cast to Jd class. */
		operator Jd () const { return Jd(to_jd()); }

		/**	Assign and convert from Jd type to Gregorian type. */
		Gregorian& operator = (Jd const& jd) { from_jd(jd.jd());  return *this; }

		std::string Gregorian::to_string() const;
	// Block some operators
	private:
		Gregorian operator + (detail::packaged_year_real const&);
		Gregorian operator - (detail::packaged_year_real const&);
		Gregorian operator + (detail::packaged_month_real const&);
		Gregorian operator - (detail::packaged_month_real const&);
	}; 

	// OPERATORS
	// =============================================

	/** Gregorian + (integer month) */
	Gregorian operator + (Gregorian const& dt, detail::packaged_month_integer const& month);

	/** Gregorian - (integer month) */
	inline Gregorian operator - (Gregorian const& dt, detail::packaged_month_integer const& month) { return dt + detail::packaged_month_integer(-month.nMonths_); }

	/**	Gregorian + (integer year) */
	Gregorian operator + (Gregorian const& dt, detail::packaged_year_integer const& year);

	/**	Gregorian - (integer year) */
	inline Gregorian operator - (Gregorian const& dt, detail::packaged_year_integer const& year) { return dt + detail::packaged_year_integer(-year.nYears_); }

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Gregorian const& g) { return os << g.to_string(); }
}