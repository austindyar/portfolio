#pragma once

/*
khronos/def.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library definitions.
*/

#include <string>
namespace khronos {
	// TYPES
	// ----------------------------------------------------------------------
	using hour_t = int;
	using minute_t = int;
	using second_t = double;

	using year_t = long long;	// year +/- 24 Billion
	using month_t = int;		// month [1..12]
	using day_t = int;			// day [1..31]

	using jd_t = double;		// Represents Julian date (JD)

	// VALUES
	// ----------------------------------------------------------------------
	constexpr double  EARTH_ORBITAL_PERIOD_DAYS = 365.256'363'004;
	constexpr second_t  SECONDS_PER_DAY = 86'400;

	const std::string WTIMEOFDAY = "w";
	const std::string NOTIMEOFDAY = "n";
}