#pragma once

/*
khronos/khronos.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-30

Khronos library header.
*/


#include <khronos/def.hpp>
#include <khronos/julian_day.hpp>
#include <khronos/timeofday.hpp>
#include <khronos/gregorian_calendar.hpp>
#include <khronos/julian_calendar.hpp>
#include <khronos/islamic_calendar.hpp>
#include <khronos/hebrew_calendar.hpp>
#include <khronos/vulcan_calendar.hpp>