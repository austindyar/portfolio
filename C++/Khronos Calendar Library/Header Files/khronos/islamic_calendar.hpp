#pragma once

/*
khronos/islamic_calendar.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Islamic calendar header.
*/

#include <khronos/calendar.hpp>
#include <khronos/julian_day.hpp>

namespace khronos {
	// VALUES
	// =============================================
	
	/**	JD of the start of the Islamic epoch. */
	jd_t constexpr ISLAMIC_EPOCH = 1'948'439.5;

	// FUNCTIONS
	// =============================================
	inline bool is_islamic_leapyear(year_t year) { return ((((11*year)) + 14) % 30) < 11; }
	jd_t islamic_to_jd(year_t year, month_t month, day_t day);
	jd_t islamic_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds);
	void jd_to_islamic(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second);
	void jd_to_islamic(jd_t jd, year_t& year, month_t& month, day_t& day);

	inline day_t days_in_month(month_t month, bool isLeapYear) { return islamic::days_in_month(month, isLeapYear); }

	inline char const* islamic_month_name(month_t month) { return islamic::month_name(month); }

	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Islamic Calendar Date class. */
	class Islamic {
		year_t	year_;
		month_t	month_;
		day_t day_;
		hour_t hour_;
		minute_t minute_;
		second_t second_;

		void from_jd(jd_t jd) { jd_to_islamic(jd, year_, month_, day_, hour_, minute_, second_); }
		jd_t to_jd() const { return islamic_to_jd(year_, month_, day_, hour_, minute_, second_); }
	public:
		Islamic();

		year_t year() const { return year_; }
		month_t month() const { return month_; }
		day_t day() const { return day_; }
		hour_t hour() const { return hour_; }
		minute_t minute() const { return minute_; }
		second_t second() const { return second_; }

		Islamic(year_t year, month_t month, day_t day) :year_(year), month_(month), day_(day), hour_(0), minute_(0), second_(0) {}

		Islamic(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec);

		/** Construct a Islamic date from Islamic Day Number object. */
		Islamic(Jd const& jd) { from_jd(jd.jd()); }

		/**	Implicit cast to Jd class. */
		operator Jd () const { return Jd(to_jd()); }

		/**	Assign and convert from Jd type to Islamic type. */
		Islamic& operator = (Jd const& jd) { from_jd(jd.jd()); return *this; }

		std::string Islamic::to_string() const;
	// Block some operators
	private:
		Islamic operator + (detail::packaged_year_real const&);
		Islamic operator - (detail::packaged_year_real const&);
		Islamic operator + (detail::packaged_month_real const&);
		Islamic operator - (detail::packaged_month_real const&);
	};

	// OPERATORS
	// =============================================

	/** Islamic + (integer month) */
	Islamic operator + (Islamic const& dt, detail::packaged_month_integer const& month);

	/** Islamic - (integer month) */
	inline Islamic operator - (Islamic const& dt, detail::packaged_month_integer const& month) { return dt + detail::packaged_month_integer(-month.nMonths_); }

	/**	Islamic + (integer year) */
	Islamic operator + (Islamic const& dt, detail::packaged_year_integer const& year);

	/**	Islamic - (integer year) */
	inline Islamic operator - (Islamic const& dt, detail::packaged_year_integer const& year) { return dt + detail::packaged_year_integer(-year.nYears_); }

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Islamic const& g) { return os << g.to_string(); }
}
