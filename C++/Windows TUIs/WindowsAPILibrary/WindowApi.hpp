/*!	\file		WindowApi.hpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#pragma once
#include <Windows.h>
#undef min

#include <sstream>

class WindowApi {
private:
	//=======================================================
	// Error exception class and utilities
	//=======================================================
	/* ErrorDescription */
	static std::string ErrorDescription(DWORD dwMessageID) {
		char* msg;
		auto c = FormatMessageA(
			/* flags */			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK,
			/* source*/			NULL,
			/* message ID */	dwMessageID,
			/* language */		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			/* buffer */		(LPSTR) &msg,
			/* size */			0,
			/* args */			NULL
		);

		std::string strMsg = (c == 0)
			? "unknown"
			: msg;
		LocalFree(msg);
		return strMsg;
	}

	/* Console error exception class. */
	class XError {
	public:
		using id_type = decltype(GetLastError());
		using file_type = char const *;
		using string_type = std::string;
	private:
		id_type code_;
		int	line_;
		file_type file_;
	public:
		XError(int line, file_type file) : code_(GetLastError()), line_(line), file_(file) {}
		auto code() const -> id_type { return code_; }
		auto line() const -> int { return line_; }
		auto file() const -> file_type { return file_; }

		string_type msg() const {
			std::ostringstream oss;
			oss << "Error: " << code() << "\n";
			oss << ErrorDescription(code()) << "\n";
			oss << "In: " << file() << "\n";
			oss << "Line: " << line() << "\n";
			return oss.str();
		}
	};
public:
	//=======================================================
	// Accessible Functions
	//=======================================================

	// Title
	DWORD GetConsoleTitleAApi(char* titleData, DWORD titleSize);
	void SetTitleApi(char* titleData);

	// Window
	void SetConsoleWindowInfoApi(HANDLE hConsoleOutput, SMALL_RECT & window);

	// Buffer
	void GetConsoleScreenBufferInfoApi(HANDLE hConsoleOutput, CONSOLE_SCREEN_BUFFER_INFO & csbi);
	void SetConsoleScreenBufferSizeApi(HANDLE hConsoleOutput, COORD csbi);

	// Cursor
	void GetConsoleCursorInfoApi(HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO & cci);
	void SetConsoleCursorInfoApi(HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO & newCCI);
	void SetConsoleCursorPositionApi(HANDLE hConsoleOutput, COORD cursorPos);

	// Fill Console
	void FillConsoleOutputCharacterAApi(HANDLE hConsoleOutput, CHAR chars, DWORD cellsToWrite, COORD firstCell, DWORD &charsWritten);
	void FillConsoleOutputAttributeApi(HANDLE hConsoleOutput, CHAR chars, DWORD cellsToWrite, COORD firstCell, DWORD &charsWritten);

	// Read Input / Output Handles
	void ReadConsoleOutputAApi(HANDLE hConsoleOutput, CHAR_INFO* bufferData, COORD csbi, COORD bufferCoord, SMALL_RECT & bufferRect);
	bool ReadConsoleInputApi(HANDLE hConsoleInput, INPUT_RECORD* inBuffer, DWORD inBufferSize, DWORD & numEvents);

	// Write Output Handle
	void WriteConsoleOutputAApi(HANDLE hConsoleOutput, CHAR_INFO* bufferData, COORD csbi, COORD bufferCoord, SMALL_RECT & bufferRect);
};