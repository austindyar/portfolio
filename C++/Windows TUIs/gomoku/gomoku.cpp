/*!	\file		gomoku.cpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#include "gomoku.hpp"

// Application
bool done = false;

BOOL CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		done = true;
		return TRUE;
	}

	return FALSE;
}

////////////////////////////////
//			Gomoku			  //
////////////////////////////////
Gomoku::Gomoku() {
	window = Window(CtrlHandler);

	windowWidth = 31;
	windowHeight = 31;
	
	CHAR_INFO charInfo;
	charInfo.Attributes = window.GetColorDefinition(Yellow);
	charInfo.Char.AsciiChar = ' ';
	defaultBackground = std::vector<CHAR_INFO>(windowWidth * windowHeight, charInfo);

	size_t count = 0, gridSize = 15, area = windowWidth * windowHeight - windowWidth;
	for (size_t index = 0; index < defaultBackground.size(); ++index) {
		if (index > windowWidth && index < area && index % windowWidth != 0 && index % 2 == 0) {
			if (count >= gridSize) {
				count = 0;
				index += windowWidth - 1;
			}
			++count;
			defaultBackground[index].Attributes = window.GetColorDefinition(White);
		}
	}
}

void Gomoku::ResetLocations() {
	redLocations.erase(redLocations.begin(), redLocations.end());
	blackLocations.erase(blackLocations.begin(), blackLocations.end());
}

int Gomoku::execute() {
	window.StoreState();

	window.ResizeWindow(windowWidth, windowHeight);
	window.SetTitle("Gomoku");
	window.HideCursor();
	window.SetBackground(defaultBackground);

	window.UseColors({ Red, Black });

	window.AddCommand(Key::LMouseClick, std::shared_ptr<Command>(new LMouseClickCommand(window, *this)));
	window.AddCommand(Key::R, std::shared_ptr<Command>(new RButtonCommand(window, *this)));

	while (!done) {
		window.ProcessInput();
	}

	window.RestoreState();
	return EXIT_SUCCESS;
}

Gomoku::~Gomoku() {
	window.RestoreState();
}

////////////////////////////////
//		LMouseClick			  //
////////////////////////////////
bool LMouseClickCommand::OutsideOfBoundsCheck(COORD mouseLocation) {
	std::vector<CHAR_INFO> background = window.GetBackground();
	recievedIndex = mouseLocation.X + (mouseLocation.Y * gomoku.windowWidth);
	if (background[recievedIndex].Attributes != window.GetColorDefinition(White)) {
		return true;
	}
	return false;
}

bool LMouseClickCommand::DidSomeoneWin(WORD color, std::vector<size_t> winningVector) {
	std::vector<CHAR_INFO> background = window.GetBackground();
	for (size_t index = 0; index < winningVector.size(); ++index) {
		for (size_t line = 0; line < winningLines.size(); ++line) {
			int inARow = 0;
			size_t currentLocation = winningVector[index];
			while (inARow != 4) {
				if (std::find(winningVector.begin(), winningVector.end(), currentLocation += winningLines[line]) != winningVector.end()) {
					++inARow;
				}
				else {
					break;
				}
			}

			if (inARow == 4) {
				return true;
			}
		}
	}
	return false;
}

void LMouseClickCommand::execute() { 
	if (!OutsideOfBoundsCheck(window.GetMouseLocation())) {
		window.FillMouseLocation();
		if(window.GetCurrentColor() == window.GetColorDefinition(Red)) {
			gomoku.redLocations.push_back(recievedIndex);
			if (DidSomeoneWin(Red, gomoku.redLocations)) {
				gomoku.ResetLocations();
				window.SetBackground(Red, gomoku.windowWidth, gomoku.windowHeight);
			}
		}
		else if(window.GetCurrentColor() == window.GetColorDefinition(Black)) {
			gomoku.blackLocations.push_back(recievedIndex);
			if (DidSomeoneWin(Black, gomoku.blackLocations)) {
				gomoku.ResetLocations();
				window.SetBackground(Black, gomoku.windowWidth, gomoku.windowHeight);
			}
		}
		window.CycleColors();
	}
}

////////////////////////////////
//		RButtonClick		  //
////////////////////////////////
void RButtonCommand::execute() {
	window.SetBackground(gomoku.defaultBackground);
	gomoku.ResetLocations();
}