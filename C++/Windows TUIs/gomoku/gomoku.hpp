/*!	\file		gomoku.hpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#pragma once

#include "ConsoleApplication.hpp"
#include "Window.hpp"

class Gomoku : ConsoleApplication {
private:
	Window window;
public:
	Gomoku();
	~Gomoku();
	int execute() override;

	size_t windowHeight;
	size_t windowWidth;
	std::vector<CHAR_INFO> defaultBackground;

	std::vector<size_t> redLocations;
	std::vector<size_t> blackLocations;
	void ResetLocations();
} gomokuExe;

class LMouseClickCommand : public Command {
	Window& window;
	Gomoku& gomoku;

	enum Direction {
		upLeftDiagnol = -64, upVertical = -62, upRightDiagnol = -60,
		left = -2, right = 2,
		downLeftDiagnol = 64, downVetical = 66, downRightDiagnol = 68
	};
	std::vector<Direction> winningLines;

	size_t recievedIndex;

	bool OutsideOfBoundsCheck(COORD mouseLocation);
	bool DidSomeoneWin(WORD, std::vector<size_t> winningVector);

public:
	LMouseClickCommand(Window& win, Gomoku& gomoku) : window(win), gomoku(gomoku) {
		winningLines = { upLeftDiagnol, upVertical, upRightDiagnol, left, right, downLeftDiagnol, downVetical, downRightDiagnol};
	}
	void execute() override;
};

class RButtonCommand : public Command {
	Window& window;
	Gomoku& gomoku;

public:
	RButtonCommand(Window& win, Gomoku& gomoku) : window(win), gomoku(gomoku) {}
	void execute() override;
};