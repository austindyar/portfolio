/*
khronos/hebrew.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Hebrew calendar class implementation.
*/

#include <khronos/hebrew_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <algorithm>
#include <cassert>
#include <sstream>
#include <ctime>
#include <iomanip>

namespace khronos {
	/**	Hebrew default constructor.  Initialize to the current local time. */
	Hebrew::Hebrew() { 
		Jd jd; 
		jd_to_hebrew(jd.jd(), year_, month_, day_, hour_, minute_, second_); 
	}

	// Hebrew 6 arg constructor
	Hebrew::Hebrew(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec) {
		year_ = year;
		month_ = month;
		day_ = day;
		hour_ = hour;
		minute_ = min;
		second_ = sec;
	}

	/** Print the Hebrew Date as a string. */
	std::string Hebrew::to_string() const {
		std::ostringstream oss;
		oss << hebrew_month_name(month_) << ' ' << (unsigned)day_ << ' ' << year_ << ", ";

		if (hour_ % 12 == 0 && hour_ != 0) { oss << hour_ << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else if (hour_ == 0) { oss << 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else { oss << hour_ % 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		if (hour_ >= 12) { oss << " pm"; }
		else { oss << " am"; }

		return oss.str();
	}
}