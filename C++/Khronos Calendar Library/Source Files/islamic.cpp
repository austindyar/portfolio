/*
khronos/islamic.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Islamic calendar class implementation.
*/

#include <khronos/islamic_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <algorithm>
#include <cassert>
#include <sstream>
#include <ctime>
#include <iomanip>

namespace khronos {
	/**	Isalmic default constructor.  Initialize to the current local time. */
	Islamic::Islamic() {
		Jd jd;
		jd_to_islamic(jd.jd(), year_, month_, day_, hour_, minute_, second_);
	}

	// Islamic 6 arg constructor
	Islamic::Islamic(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec) {
		year_ = year;
		month_ = month;
		day_ = day;
		hour_ = hour;
		minute_ = min;
		second_ = sec;

	}

	/** Print the Islamic Date as a string. */
	std::string Islamic::to_string() const {
		std::ostringstream oss;

		if (hour_ >= 0 || hour_ < 12) { oss << islamic::day_name(islamic::day_of_week(to_jd()) + 1) << ", "; }
		else { oss << islamic::day_name(islamic::day_of_week(to_jd())) << ", "; }
			
		oss << islamic_month_name(month_) << ' ' << (unsigned)day_ << ' ' << year_ << ", ";

		if (hour_ % 12 == 0 && hour_ != 0) { oss << hour_ << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else if (hour_ == 0) { oss << 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else { oss << hour_ % 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		if (hour_ >= 12) { oss << " pm"; }
		else { oss << " am"; }

		return oss.str();
	}

	/** Islamic + (integer year) operator. */
	Islamic operator + (Islamic const& dt, detail::packaged_year_integer const& year) {
		year_t y = dt.year() + year.nYears_;
		month_t m = dt.month();
		day_t d = dt.day();

		if (m == 12 && d == 30 && !is_islamic_leapyear(y)){ d = 29; }

		return Islamic(y, m, d, dt.hour(), dt.minute(), dt.second());
	}


	/** Islamic + (integer month) operator. */
	Islamic operator + (Islamic const& date, detail::packaged_month_integer const& month) {
		year_t y = date.year() + (month.nMonths_ / 12);
		month_t m = date.month() + (month.nMonths_ % 12);
		int adjustment = ((m - 1) / 12) + ((m - 12) / 12);
		y = y + adjustment;
		m = m - (adjustment * 12);
		day_t d = std::min(date.day(), islamic::days_in_month(m, is_islamic_leapyear(y)));
		return Islamic(y, m, d, date.hour(), date.minute(), date.second());
	}
}