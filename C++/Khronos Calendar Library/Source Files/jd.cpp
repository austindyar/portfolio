/*
khronos/jd.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Julian day number class implementation.
*/

#include <khronos/julian_day.hpp>
#include <khronos/gregorian_calendar.hpp>
#include <iomanip>
#include <sstream>
#include <string>
#include <ctime>
using namespace std;

namespace khronos {
	/**	Jdn default constructor.  Initialize to the current local time. */
	Jd::Jd() : jd_(0) {
		time_t nowTime = time(NULL);
		struct tm tmNow;
		localtime_s(&tmNow, &nowTime);
		jd_ = gregorian_to_jd(tmNow.tm_year + 1900, month_t(tmNow.tm_mon + 1), day_t(tmNow.tm_mday), hour_t(tmNow.tm_hour), minute_t(tmNow.tm_min), second_t(tmNow.tm_sec));
	}

	/**	Jd WTIMEOFDAY/NOTIMEOFDAY constructor. */
	Jd::Jd(std::string input) {
		time_t nowTime = time(NULL);
		struct tm tmNow;
		localtime_s(&tmNow, &nowTime);
		if (input[0] == 'w') { jd_ = gregorian_to_jd(tmNow.tm_year + 1900, month_t(tmNow.tm_mon + 1), day_t(tmNow.tm_mday), hour_t(tmNow.tm_hour), minute_t(tmNow.tm_min), second_t(tmNow.tm_sec)); }
		else { jd_ = gregorian_to_jd(tmNow.tm_year + 1900, month_t(tmNow.tm_mon + 1), day_t(tmNow.tm_mday)); }
	}

	/** Print the Julian Day as a string. */
	std::string Jd::to_string() const { ostringstream oss; oss << "JD " << jd_; return oss.str(); }
};