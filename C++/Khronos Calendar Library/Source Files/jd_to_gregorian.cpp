/*
khronos/jd_to_gregorian.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'gregorian::from_jdn' implementation.
*/

#include <khronos/gregorian_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <cassert>

namespace khronos {
	void jd_to_gregorian(jd_t jd, year_t& year, month_t& month, day_t& day) {
		jd = floor(jd - 0.5) + 0.5;
		long long a = static_cast<long long>(jd + 0.5) + 32044;
		long long b = (4 * a + 3) / 146097;
		long long c = a - b * 146097 / 4;
		long long d = (4 * c + 3) / 1461;
		long long e = c - 1461 * d / 4;
		long long m = (5 * e + 2) / 153;
		day = static_cast<day_t>(e - (153 * m + 2) / 5 + 1);
		month = static_cast<month_t>(m + 3 - 12 * (m / 10));
		year = b * 100 + d - 4800 + m / 10;
	}

	void jd_to_gregorian(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second) {
		jd_to_gregorian(jd, year, month, day);
		auto timeOfDay = (jd + 0.5) - floor(jd + 0.5);
		hms(timeOfDay, hour, minute, second);
	}
}