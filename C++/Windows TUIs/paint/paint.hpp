/*!	\file		paint.hpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#pragma once

#include "Window.hpp"
#include "ConsoleApplication.hpp"

class Paint : ConsoleApplication {
private:
	Window window;
public:
	Paint();
	~Paint();
	size_t windowHeight;
	size_t windowWidth;
	int execute() override;
} paintExe;

class LMouseClickCommand : public Command {
	Window& window;
public:
	LMouseClickCommand(Window& win) : window(win) {}
	void execute() override;
};

class RMouseClickCommand : public Command {
	Window& window;
public:
	RMouseClickCommand(Window& win) : window(win) {}
	void execute() override;
};

class CButtonCommand : public Command {
	Window& window;
	Paint& paint;
public:
	CButtonCommand(Window& win, Paint& paint) : window(win), paint(paint) {}
	void execute() override;
};