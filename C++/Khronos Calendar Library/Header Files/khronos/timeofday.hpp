#pragma once

/*
khronos/timeofday.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'time-of-day' declarations.
*/

#include <khronos/def.hpp>
#include <khronos/utility.hpp>
#include <tuple>

namespace khronos {
	// Input hour + AM / PM
	// ======================================================================
	using namespace utility;
	constexpr hour_t operator ""_am(unsigned long long inputTime) {
		return utility::mod(static_cast<hour_t>(inputTime), 12) == 0 ? 0 : static_cast<hour_t>(inputTime);
	}
	constexpr hour_t operator ""_AM(unsigned long long inputTime) {
		return utility::mod(static_cast<hour_t>(inputTime), 12) == 0 ? 0 : static_cast<hour_t>(inputTime);
	}


	constexpr hour_t operator ""_pm(unsigned long long inputTime) {
		return utility::mod(static_cast<hour_t>(inputTime), 12) == 0 ? 12 : static_cast<hour_t>(inputTime) + 12;
	}
	constexpr hour_t operator ""_PM(unsigned long long inputTime) {
		return utility::mod(static_cast<hour_t>(inputTime), 12) == 0 ? 12 : static_cast<hour_t>(inputTime) + 12;
	}

	/** Time of day. */
	inline double tod(hour_t hour, minute_t minute, second_t second) { 
		return ((hour * 60 + minute) * 60 + second) / (24 * 60 * 60);
	}

	inline jd_t tod(jd_t jd) { return (jd + 0.5) - floor(jd + 0.5); }

	/** Hour / Minute / Second. */
	void hms(jd_t tod, hour_t& hour, minute_t& minutes, second_t& seconds);
};