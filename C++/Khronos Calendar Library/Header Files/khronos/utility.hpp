#pragma once

/*
khronos/utility.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Utility library header.
*/

#include <cmath>

namespace khronos {
	namespace utility {
		inline double mod(double a, double b) { return a - b * floor(a / b); }
		inline double jwday(double j) { return mod(floor(j + 1.5), 7.0); }
	}
}