#pragma once

/*
khronos/hebrew_calendar.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Hebrew calendar header.
*/

#include <khronos/calendar.hpp>
#include <khronos/julian_day.hpp>
#include <khronos/utility.hpp>

namespace khronos {
	// VALUES
	// =============================================

	/**	JD of the start of the Hebrew epoch. */
	jd_t constexpr HEBREW_EPOCH = 347'995.5;

	// FUNCTIONS
	// =============================================
	inline bool is_hebrew_leapyear(year_t year) { return mod(7.0 * (double)year + 1.0, 19.0) < 7; }
	jd_t hebrew_to_jd(year_t year, month_t month, day_t day);
	jd_t hebrew_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds);
	void jd_to_hebrew(jd_t jd, year_t& year, month_t& month, day_t& day);
	void jd_to_hebrew(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second);

	inline jd_t daysInYear(year_t year) { return hebrew_to_jd(year + 1, 7, 1) - hebrew_to_jd(year, 7, 1); }
	inline month_t hebrew_months_in_year(year_t year) { month_t months = 12; if (is_hebrew_leapyear(year)) months += 1; return months; }
	inline char const* hebrew_month_name(month_t month) { return hebrew::month_name(month); }

	inline day_t delayOfWeek(year_t year) {
		double months = floor(((235 * year) - 234) / 19.0);
		double parts = 12'084 + 13'753 * months;
		double days = months * 29 + floor(parts / 25'920);
		return static_cast<day_t>(mod(3 * (days + 1), 7) < 3 ? days + 1 : days);
	}

	inline day_t delayAdjacentYear(year_t year) {
		int last = delayOfWeek(year - 1);
		int present = delayOfWeek(year);
		int next = delayOfWeek(year + 1);
		int days = 0;
		if (next - present == 356)
			return days = 2;
		if (next - present != 356 && present - last == 382)
			return days = 1;
		return days;
	}


	inline day_t hebrew_days_in_month(year_t year, month_t month) {
		switch (month)
		{
		case 2: case 4: case 6: case 10: case 13:
			return 29;
		case 12:
			if (!is_hebrew_leapyear(year))
				return 29;
		case 8:
			if (mod(daysInYear(year), 10) != 5.0)
				return 29;
		case 9:
			if (mod(daysInYear(year), 10) == 3.0)
				return 29;
		}
		return 30;
	}

	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Hebrew Calendar Date class. */
	class Hebrew {
		year_t	year_;
		month_t	month_;
		day_t day_;
		hour_t hour_;
		minute_t minute_;
		second_t second_;

		/** From and to JD. */
		void from_jd(jd_t jd) { jd_to_hebrew(jd, year_, month_, day_, hour_, minute_, second_); }
		jd_t to_jd() const { return hebrew_to_jd(year_, month_, day_, hour_, minute_, second_); }
	public:
		Hebrew();

		year_t year() const { return year_; }
		month_t month() const { return month_; }
		day_t day() const { return day_; }
		hour_t hour() const { return hour_; }
		minute_t minute() const { return minute_; }
		second_t second() const { return second_; }

		/** Constructors. */
		Hebrew(year_t year, month_t month, day_t day) : year_(year), month_(month), day_(day), hour_(0), minute_(0), second_(0) {}
		Hebrew(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec);

		/** Construct a Hebrew date from Julian Day Number object */
		Hebrew(Jd const& jd) { from_jd(jd.jd()); }

		/**	Implicit cast to Jd class. */
		operator Jd () const { return Jd(to_jd()); }

		/**	Assign and convert from Hebrew type to Julian type. */
		Hebrew& operator = (Jd const& jd) { from_jd(jd.jd()); return *this; }

		std::string Hebrew::to_string() const;
	// block some operators
	private:
		Hebrew operator + (detail::packaged_year_real const&);
		Hebrew operator - (detail::packaged_year_real const&);
		Hebrew operator + (detail::packaged_month_real const&);
		Hebrew operator - (detail::packaged_month_real const&);
	};

	// OPERATORS
	// =============================================

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Hebrew const& g) { return os << g.to_string(); }
}
