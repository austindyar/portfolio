#pragma once

/*
khronos/vulcan.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Vulcan calendar header.
*/

#include <khronos/calendar.hpp>
#include <khronos/julian_day.hpp>
#include <khronos/utility.hpp>

namespace khronos {
	// VALUES
	// =============================================

	/**	JD of the start of the Vulcan epoch. */
	jd_t constexpr VULCAN_EPOCH = 173'651.6;
	jd_t constexpr VULCAN_CONVERSION = 0.6075;
	constexpr second_t VULCAN_SECONDS_PER_DAY = 52'488;

	// FUNCTIONS
	// =============================================

	void vulcan_hms(jd_t tod, hour_t& hour, minute_t& minutes, second_t& seconds);
	jd_t vulcan_to_jd(year_t year, month_t month, day_t day);
	jd_t vulcan_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds);
	void jd_to_vulcan(jd_t jd, year_t& year, month_t& month, day_t& day);
	void jd_to_vulcan(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second);

	inline jd_t vulcan_days_in_year(year_t year) { return 252; }

	inline month_t vulcan_months_in_year(year_t year) { return 12; }

	inline day_t vulcan_days_in_month() { return 21; }

	inline char const* vulcan_month_name(month_t month) { return vulcan::month_name(month); }

	inline double vulcan_tod(hour_t hour, minute_t minute, second_t second) { return (((hour * 54 + minute) * 54 + second) / (18 * 54 * 54) )* (266.4/252); }

	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Vulcan Calendar Date class. */
	class Vulcan {
		year_t	year_;
		month_t	month_;
		day_t day_;
		hour_t hour_;
		minute_t minute_;
		second_t second_;

		void from_jd(jd_t jd) { jd_to_vulcan(jd, year_, month_, day_, hour_, minute_, second_); }
		jd_t to_jd() const { return vulcan_to_jd(year_, month_, day_, hour_, minute_, second_); }
	public:
		Vulcan();

		year_t year() const { return year_; }
		month_t month() const { return month_; }
		day_t day() const { return day_; }
		hour_t hour() const { return hour_; }
		minute_t minute() const { return minute_; }
		second_t second() const { return second_; }

		Vulcan(year_t year, month_t month, day_t day):year_(year), month_(month), day_(day), hour_(0), minute_(0), second_(0) {}
		Vulcan(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec);

		/** Construct a Vulcan date from Julian Day Number object. */
		Vulcan(Jd const& jd) { from_jd(jd.jd()); }

		/**	Implicit cast to Jd class. */
		operator Jd () const { return Jd(to_jd()); }

		/**	Assign and convert from Vulcan type to Julian type. */
		Vulcan& operator = (Jd const& jd) { from_jd(jd.jd()); return *this; }

		std::string Vulcan::to_string() const;
	// Block some operators
	private:
		Vulcan operator + (detail::packaged_year_real const&);
		Vulcan operator - (detail::packaged_year_real const&);
		Vulcan operator + (detail::packaged_month_real const&);
		Vulcan operator - (detail::packaged_month_real const&);
	};

	// OPERATORS
	// =============================================

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Vulcan const& g) { return os << g.to_string(); };
	
	/** Day addition/subtraction. */
	inline Vulcan operator + (Vulcan const& vulcan, detail::packaged_day days) {
		jd_t testDay = days.nDays_;
		jd_t testMonth = 0;

		if (testDay > 21) { testMonth = mod(testDay, 21); testDay -= testMonth * 21; }

		jd_t testYear = 0;
		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<year_t>((vulcan.year() + testYear)), static_cast<month_t>((vulcan.month() + testMonth)), static_cast<day_t>((vulcan.day() + testDay)), (vulcan.hour()),(vulcan.minute()),(vulcan.second())); }

	inline Vulcan operator - (Vulcan const& vulcan, detail::packaged_day days) {
		jd_t testDay = days.nDays_;
		jd_t testMonth = 0;

		if (testDay > 21) { testMonth = mod(testDay, 21); testDay -= testMonth * 21; }

		jd_t testYear = 0;
		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<year_t>((vulcan.year() - testYear)), static_cast<month_t>((vulcan.month() - testMonth)), static_cast<day_t>((vulcan.day() - testDay)), (vulcan.hour()), (vulcan.minute()), (vulcan.second()));
	}

	//** Month addition/subtraction. */
	inline Vulcan operator + (Vulcan const& vulcan, detail::packaged_month_integer months) {
		jd_t testMonth = months.nMonths_;
		jd_t testYear = 0;

		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<year_t>((vulcan.year() + testYear)), static_cast<month_t>((vulcan.month() + testMonth)), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second()));
	}

	inline Vulcan operator - (Vulcan const& vulcan, detail::packaged_month_integer months) {
		jd_t testMonth = months.nMonths_;
		jd_t testYear = 0;

		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<year_t>((vulcan.year() - testYear)), static_cast<month_t>((vulcan.month() - testMonth)), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second()));
	}

	inline Vulcan operator + (Vulcan const& vulcan, detail::packaged_month_real months) {
		jd_t testMonth = months.nMonths_;
		jd_t testYear = 0;

		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<month_t>((vulcan.year() + testYear)), static_cast<month_t>((vulcan.month() + testMonth)), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second()));
	}

	inline Vulcan operator - (Vulcan const& vulcan, detail::packaged_month_real months) {
		jd_t testMonth = months.nMonths_;
		jd_t testYear = 0;

		if (testMonth > 12) { testYear = mod(testMonth, 12.0); testMonth -= testYear * 12.0; }

		return Vulcan(static_cast<year_t>((vulcan.year() - testYear)), static_cast<month_t>((vulcan.month() - testMonth)), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second()));
	}

	//** Year addition/subtraction. */
	inline Vulcan operator + (Vulcan const& vulcan, detail::packaged_year_integer years) {
		return Vulcan(static_cast<year_t>((vulcan.year() + years.nYears_)), (vulcan.month()), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second())); }

	inline Vulcan operator - (Vulcan const& vulcan, detail::packaged_year_integer years) {
		return Vulcan(static_cast<year_t>((vulcan.year() - years.nYears_)), (vulcan.month()), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second())); }

	inline Vulcan operator + (Vulcan const& vulcan, detail::packaged_year_real years) {
		return Vulcan(static_cast<year_t>((vulcan.year() + years.nYears_)), (vulcan.month()), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second())); }

	inline Vulcan operator - (Vulcan const& vulcan, detail::packaged_year_real years) {
		return Vulcan(static_cast<year_t>((vulcan.year() - years.nYears_)), (vulcan.month()), (vulcan.day()), (vulcan.hour()), (vulcan.minute()), (vulcan.second())); }
}