/*!	\file		Window.hpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#pragma once

#include "../../WindowsAPILibrary/WindowApi.hpp"
#include <map>
#include <vector>
#include <algorithm>
#include <iostream>

enum Key { C = 0, R, LMouseClick, RMouseClick };

enum Color { Red, Green, Blue, Yellow, Black, White };

/*!	\brief	ConsoleApplication is the base-class of the framework.
*/
class Command {
public:
	virtual void execute() { }
	virtual ~Command(){}
};

class Window {
	// Api
	WindowApi thisWindowApi;

	// Commands
	std::map<int, Key> asciiToKey;
	std::map<Key, std::shared_ptr<Command>> availableCommands;

	// Event Records
	MOUSE_EVENT_RECORD mouseEvent;
	KEY_EVENT_RECORD keyEvent;

	// Colors
	std::map<Color, CHAR> availableColors;
	std::vector<CHAR> usedColors;
	size_t currentColor;

	// Handles
	HANDLE hConsoleInput, hConsoleOutput;

	// Buffer
	std::vector<INPUT_RECORD> inBuffer;

	// Original System Data
	std::vector<char>			originalTitle;
	CONSOLE_SCREEN_BUFFER_INFO	originalCSBI;
	CONSOLE_CURSOR_INFO			originalCCI;
	std::vector<CHAR_INFO>		originalBuffer;
	COORD						originalBufferCoord;
	DWORD						originalConsoleMode;
private:
	// Input Key Events
	void MouseEventProc(MOUSE_EVENT_RECORD const& mer);
	void KeyEventProc(KEY_EVENT_RECORD const& ker);

public:
	Window() {}
	Window(BOOL(*handler)(DWORD));
	virtual ~Window() {}

	// Window
	void StoreState();
	void ResizeWindow(size_t width, size_t height);
	void SetTitle(std::string title);
	void RestoreState();

	// Background
	void SetBackground(std::vector<CHAR_INFO> background);
	void SetBackground(Color color, size_t width, size_t height);
	std::vector<CHAR_INFO> GetBackground();

	// Color
	void UseColors(std::vector<Color> colors);
	void CycleColors();
	WORD GetColorDefinition(Color color);
	CHAR GetCurrentColor();

	// Command
	void AddCommand(Key key, std::shared_ptr<Command> const& command);

	// Mouse
	COORD GetMouseLocation();
	void FillMouseLocation();

	// Cursor
	void HideCursor();

	// Input
	void ProcessInput();
};