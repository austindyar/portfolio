/*
khronos/julian.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Julian calendar class implementation.
*/

#include <khronos/julian_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <algorithm>
#include <cassert>
#include <sstream>
#include <ctime>
#include <iomanip>

namespace khronos {
	/**	Julian default constructor.  Initialize to the current local time. */
	Julian::Julian() {
		Jd jd;
		jd_to_julian(jd.jd(), year_, month_, day_, hour_, minute_, second_);
	}

	// Julian 6 arg constructor
	Julian::Julian(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec) {
		year_ = year;
		month_ = month;
		day_ = day;
		hour_ = hour;
		minute_ = min;
		second_ = sec;
	}

	/** Print the Julian Date as a string. */
	std::string Julian::to_string() const {
		std::ostringstream oss;
		oss << civil::day_name(day_of_week(to_jd())) << ", " << julian_month_name(month_) << ' ' << (unsigned)day_ << ' ';

		if (year_ <= 0) { oss << (-year_ + 1) << " BC, "; }
		else { oss << year_ << " AD, "; }


		if (hour_ % 12 == 0 && hour_ != 0) { 
			oss << hour_ << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; 
		}

		else if (hour_ == 0) { 
			oss << 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; 
		}

		else { 
			oss << hour_ % 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; 
		}


		if (hour_ >= 12) { oss << " pm"; }
		else { oss << " am"; }

		return oss.str();
	}

	/** Julian + (integer year) operator. */
	Julian operator + (Julian const& dt, detail::packaged_year_integer const& year) {
		year_t y = dt.year() + year.nYears_;
		month_t m = dt.month();
		day_t d = dt.day();

		if (m == February && d == 29 && !is_julian_leapyear(y)) { d = 28; }

		return Julian(y, m, d, dt.hour(), dt.minute(), dt.second());
	}

	/** Julian + (integer month) operator. */
	Julian operator + (Julian const& date, detail::packaged_month_integer const& month) {
		year_t y = date.year() + (month.nMonths_ / 12);
		month_t m = date.month() + (month.nMonths_ % 12);
		int adjustment = (m - 1) / 12 + (m - 12) / 12;
		y += adjustment;
		m -= month_t(adjustment * 12);
		day_t d = std::min(date.day(), julian_days_in_month(m, is_julian_leapyear(y)));
		return Julian(y, m, d, date.hour(), date.minute(), date.second());
	}
}