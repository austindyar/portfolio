/*
khronos/gregorian.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Gregorian calendar class implementation.
*/

#include <khronos/gregorian_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <cassert>
#include <sstream>
#include <ctime>
#include <iomanip>
#include <algorithm>

namespace khronos {
	bool is_gregorian_leapyear(year_t year) { return (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0); }

	/**	Gregorian default constructor. Initialize to the current local time. */
	Gregorian::Gregorian() {
		time_t nowTime = time(NULL);
		struct tm tmNow;
		localtime_s(&tmNow, &nowTime);
		year_ = tmNow.tm_year + 1900;
		month_ = month_t(tmNow.tm_mon + 1);
		day_ = day_t(tmNow.tm_mday);
		hour_ = hour_t(tmNow.tm_hour);
		minute_ = minute_t(tmNow.tm_min);
		second_ = second_t(tmNow.tm_sec);
	}

	// Gregorian 6 arg constructor
	Gregorian::Gregorian(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec) {
		year_ = year;
		month_ = month;
		day_ = day;
		hour_ = hour;
		minute_ = min;
		second_ = sec;
	}

	/** Print the Gregorian Date as a string. */
	std::string Gregorian::to_string() const {
		std::ostringstream oss;
		oss << civil::day_name(day_of_week(to_jd())) << ", ";
		oss << gregorian_month_name(month_) << ' ' << (unsigned)day_ << ' ';

		if (year_ <= 0) { oss << (-year_ + 1) << " BCE, "; }
		else { oss << year_ << " CE, "; }
			

		if (hour_ % 12 == 0 && hour_ != 0) { oss << hour_ << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else if (hour_ == 0) { oss << 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }

		else { oss << hour_ % 12 << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_; }
		
		if (hour_ >= 12) { oss << " pm"; }
		else { oss << " am"; }

		return oss.str();
	}

	/** Gregorian + (integer year) operator. */
	Gregorian operator + (Gregorian const& dt, detail::packaged_year_integer const& year) {
		year_t y = dt.year() + year.nYears_;
		month_t m = dt.month();
		day_t d = dt.day();

		if (m == February && d == 29 && !is_gregorian_leapyear(y)) { d = 28; }

		return Gregorian(y, m, d, dt.hour(), dt.minute(), dt.second());
	}

	/** Gregorian + (integer month) operator. */
	Gregorian operator + (Gregorian const& date, detail::packaged_month_integer const& month) {
		year_t yearsToAdd = month.nMonths_ / 12;
		month_t monthsToAdd = month.nMonths_ % 12;
		year_t y = date.year() + yearsToAdd;
		month_t m = date.month() + monthsToAdd;
		int adjustment = (m - 1) / 12 + (m - 12) / 12;
		y += adjustment;
		m -= month_t(adjustment * 12);
		day_t d = std::min(date.day(), gregorian_days_in_month(m, is_gregorian_leapyear(y)));
		return Gregorian(y, m, d, date.hour(), date.minute(), date.second());
	}
}