/*!	\file		ConsoleApplication.cpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#include "ConsoleApplication.hpp"
#include <iostream>
#include <exception>

int wmain(int argc, wchar_t* argv[]) try {
#ifdef _DEBUG
	// Enable CRT memory leak checking.
	int dbgFlags = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	dbgFlags |= _CRTDBG_CHECK_ALWAYS_DF;
	dbgFlags |= _CRTDBG_DELAY_FREE_MEM_DF;
	dbgFlags |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(dbgFlags);
#endif
	return ConsoleApplication::thisApp_sm->wmain(argc, argv);
} catch (char const * msg) {
	std::wcerr << L"exception string: " << msg << std::endl;
} catch (std::exception const& e) {
	std::wcerr << L"std::exception: " << e.what() << std::endl;
} catch (...) {
	std::wcerr << L"Error: an exception has been caught...\n";
	return EXIT_FAILURE;
}

ConsoleApplication* ConsoleApplication::thisApp_sm = nullptr;

int ConsoleApplication::wmain(int argc, wchar_t* argv[]) {
	args_m.assign(argv, argv + argc);
	return execute();
} 

ConsoleApplication::ConsoleApplication() {
	if (thisApp_sm) {
		throw std::logic_error("Error: ConsoleApplication already initialized!");
	}
	thisApp_sm = this;
}