USE Project3;
GO

CREATE TYPE dbo.DepartmentType AS TABLE (DepartmentName NVARCHAR(50), DepartmentDesc NVARCHAR(100));
GO

CREATE PROCEDURE dbo.DepartmentInsertProcedure
	@DepartmentParam dbo.DepartmentType READONLY
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	BEGIN TRANSACTION;

	INSERT INTO dbo.Departments (DepartmentName, DepartmentDesc)
	SELECT * FROM @DepartmentParam;

	COMMIT TRANSACTION DepartmentInsertProcedure;
END;

DECLARE @DepartmentVariable dbo.DepartmentType;
INSERT INTO @DepartmentVariable VALUES 
	('QA', 'Quality Assurance'), 
	('Engineering', 'Systems design and development'), 
	('Support', 'Product support');
 EXEC dbo.DepartmentInsertProcedure @DepartmentVariable;
GO

-- SELECT * FROM dbo.Departments;

CREATE OR ALTER FUNCTION dbo.GetEmployeeID (
	@FirstNameParameter NVARCHAR(50),
	@LastNameParameter NVARCHAR(50)
	)
RETURNS INT
AS
BEGIN;
	DECLARE @EmployeeID INT;

	SELECT @EmployeeID = @EmployeeID
	FROM Project3.dbo.Employees
	WHERE LastName = @LastNameParameter AND FirstName = @FirstNameParameter;

	RETURN @EmployeeID;
END;
GO

CREATE PROCEDURE dbo.InsertEmployeeProcedure
	@DepartmentIDParameter INT,
	@FirstNameParameter NVARCHAR,
	@LastNameParameter NVARCHAR,
	@SalaryParameter MONEY = 30000,
	@ManagerFirstNameParameter NVARCHAR,
	@ManagerLastNameParameter NVARCHAR
	
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRANSACTION;

		DECLARE @ManagerIDVariable INT = dbo.GetEmployeeID(
			@ManagerFirstNameParameter,
			@ManagerLastNameParameter
		);

		IF @ManagerIDVariable IS NULL
			BEGIN;
				INSERT INTO dbo.Employees(FirstName, LastName) VALUES (@ManagerFirstNameParameter, @ManagerLastNameParameter);
				SET @ManagerIDVariable = dbo.GetEmployeeID(@ManagerFirstNameParameter, @ManagerLastNameParameter);
			END;

		INSERT INTO dbo.Employees(FirstName, LastName, Salary, ManagerEmployeeID, DepartmentID) VALUES (@FirstNameParameter, @LastNameParameter, @SalaryParameter, @ManagerIDVariable, @DepartmentIDParameter);

	COMMIT TRANSACTION InsertEmployeeProc;
END;
GO

WITH EmployeeSalary(FirstName, LastName, Salary, DepartmentID, DepartmentName) AS (
	SELECT e.FirstName, 
		e.LastName, 
		e.Salary, 
		d.DepartmentID, 
		d.DepartmentName
	FROM Employees e
	INNER JOIN Departments d
	ON d.DepartmentID = e.DepartmentID
)
	
SELECT LastName, FirstName, Salary, DepartmentName, 
	LAG(Salary) OVER (PARTITION BY DepartmentID ORDER BY Salary DESC) AS NextHighestSalary,
	LAG(FirstName) OVER (PARTITION BY DepartmentID ORDER BY Salary DESC) AS FirstName, 
	LAG(LastName) OVER (PARTITION BY DepartmentID ORDER BY Salary DESC) AS LastName
FROM EmployeeSalary;

WITH DirectReports(LastName, FirstName, EmployeeID, EmployeeLevel, ManagerChain) AS (  
    SELECT LastName,
		FirstName,
		EmployeeID,
		0 AS EmployeeLevel,
		CAST(' '  AS NVARCHAR(MAX)) AS ManagerChain
    FROM dbo.Employees
    WHERE ManagerEmployeeID IS NULL

    UNION ALL
    SELECT e.LastName, 
		e.FirstName,
		e.EmployeeID, 
		EmployeeLevel + 1,
		d.ManagerChain  + CAST(CONCAT(d.LastName, ' ', d.FirstName)   AS NVARCHAR(MAX)) + ', ' AS ManagerChain
    FROM dbo.Employees AS e
        INNER JOIN DirectReports AS d 
			ON e.ManagerEmployeeID = d.EmployeeID
)  
SELECT dr.LastName AS "Employee Last Name", 
	dr.FirstName "Employee First Name",
	d.DepartmentName, 
	EmployeeLevel, "Manager Chain" = 
	CASE
		WHEN ManagerChain IS NULL THEN ' '
		WHEN ManagerChain != ' ' THEN LEFT(ManagerChain, LEN(ManagerChain) - 1)
	END
FROM DirectReports dr
INNER JOIN dbo.Employees AS e 
	ON e.EmployeeID = dr.EmployeeID 
INNER JOIN dbo.Departments AS d 
	ON e.DepartmentID = d.DepartmentID
ORDER BY EmployeeLevel DESC;
GO