/*
khronos/hebrew_to_jd.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'hebrew::to_jdn' && 'hebrew::from_jdn' implementation.
*/

#include <khronos/hebrew_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <algorithm>
#include <cassert>

namespace khronos {
	jd_t hebrew_to_jd(year_t year, month_t month, day_t day) {
		jd_t jdn = HEBREW_EPOCH + delayOfWeek(year) + delayAdjacentYear(year) + day + 1;
		if (month < 7) {
			for (year_t i = 7; i <= hebrew_months_in_year(year); ++i)
				jdn += hebrew_days_in_month(year, static_cast<month_t>(i));
			for (month_t i = 1; i <= month - 1; ++i)
				jdn += hebrew_days_in_month(year, i);
		}
		if (month >= 7)
			for (month_t i = 7; i <= month - 1; ++i)
				jdn += hebrew_days_in_month(year, i);
		return jdn;
	}

	jd_t hebrew_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds) {
		jd_t jdn = hebrew_to_jd(year, month, day);
		double timeOfDay = tod(hour, minute, seconds);
		if (timeOfDay >= 0.5) {
			timeOfDay -= 1;
		}
		return jdn + timeOfDay;
	}

	void jd_to_hebrew(jd_t jd, year_t& year, month_t& month, day_t& day) {
		jd = floor(jd) + 0.5;
		double count = floor(((jd - HEBREW_EPOCH) * 98'496) / 35'975'351);
		year = static_cast<year_t>(count - 1);
		int i = (int)count;
		while (jd >= hebrew_to_jd(i, 7, 1)) { year += 1; i += 1; }
		double first = 1;
		if (jd < hebrew_to_jd(year, 1, 1)){ first = 7; }
		month = (month_t)first;
		i = (int)first;
		while (jd > hebrew_to_jd(year, i, hebrew_days_in_month(year, i))) { month += 1; i += 1; }
		day = (day_t)floor(jd - hebrew_to_jd(year, month, 1) + 1);
	}

	void jd_to_hebrew(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second) {
		jd_to_hebrew(jd, year, month, day); hms(tod(jd), hour, minute, second);
	}
}