/*
khronos/islamic_to_jd.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'islamic::to_jdn' implementation.
*/

#include <khronos/islamic_calendar.hpp>
#include <khronos/timeofday.hpp>
#include <algorithm>
#include <cassert>

namespace khronos {
	jd_t islamic_to_jd(year_t year, month_t month, day_t day) { 
		return day + ceil(29.5 * (month - 1)) + (year - 1) * 354 + floor((3 + 11 * year) / 30.0) + ISLAMIC_EPOCH - 1;
	}

	jd_t islamic_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds) {
		jd_t jdn = islamic_to_jd(year, month, day);
		double timeOfDay = tod(hour, minute, seconds);

		if (timeOfDay >= 0.5) { timeOfDay -= 1; }

		return jdn + timeOfDay;
	}

	void jd_to_islamic(jd_t jd, year_t& year, month_t& month, day_t& day) {
		jd = floor(jd) + 0.5;
		year = static_cast<year_t>(floor((30.0 * (jd - ISLAMIC_EPOCH) + 10646) / 10631.0));
		month = static_cast<month_t>(std::min(12.0, ceil((jd - (29 + (islamic_to_jd(year, 1, 1)))) / 29.5) + 1));
		day = static_cast<day_t>(jd - islamic_to_jd(year, month, 1) + 1);
	}

	void jd_to_islamic(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second) {
		jd_to_islamic(jd, year, month, day); hms(tod(jd), hour, minute, second);
	}
}