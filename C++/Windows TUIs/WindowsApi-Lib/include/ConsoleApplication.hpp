/*!	\file		ConsoleApplication.hpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#pragma once
#pragma comment(linker,"/ENTRY:wmainCRTStartup")

#if defined (_DEBUG) && defined(_DLL)
#pragma comment (lib, "WindowsApi-Lib-Mt-Gd.lib")
#elif !defined(_DEBUG) && defined(_DLL)
#pragma comment (lib, "WindowsApi-Lib-Mt.lib")
#endif

// Setup DBG environment
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>

// Create a dbg version of new that provides more information
#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#else
#define DBG_NEW new
#endif

#include <string>
#include <vector>

class ConsoleApplication {
	using Args = std::vector<std::wstring>;
	static ConsoleApplication* thisApp_sm;
	friend int wmain(int argc, wchar_t* argv[]);
	int wmain(int argc, wchar_t* argv[]);
	Args args_m;
protected:
	ConsoleApplication();
	virtual ~ConsoleApplication() {}
	virtual int execute() = 0;
	Args const& get_args() const { return args_m; }
};