/*!	\file		WindowApi.cpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#include <Windows.h>
#undef min

#include "WindowApi.hpp"

/* Console error exception throw helper macro. */
#define THROW_IF_CONSOLE_ERROR(res) if(!res) throw XError(__LINE__,__FILE__)
#define THROW_CONSOLE_ERROR() throw XError(__LINE__,__FILE__)

// Title
DWORD WindowApi::GetConsoleTitleAApi(char* titleData, DWORD titleSize) {
	return GetConsoleTitleA(titleData, titleSize);
}

void WindowApi::SetTitleApi(char* titleData) {
	THROW_IF_CONSOLE_ERROR(SetConsoleTitleA(titleData));
}


// Window
void WindowApi::SetConsoleWindowInfoApi(HANDLE hConsoleOutput, SMALL_RECT & window) {
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &window));
}


// Buffer
void WindowApi::GetConsoleScreenBufferInfoApi(HANDLE hConsoleOutput, CONSOLE_SCREEN_BUFFER_INFO & csbi) {
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &csbi));
}

void WindowApi::SetConsoleScreenBufferSizeApi(HANDLE hConsoleOutput, COORD csbi) {
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, csbi));
}


// Cursor
void WindowApi::GetConsoleCursorInfoApi(HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO & cci) {
	THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &cci));
}

void WindowApi::SetConsoleCursorInfoApi(HANDLE hConsoleOutput, CONSOLE_CURSOR_INFO & newCCI) {
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &newCCI));
}

void WindowApi::SetConsoleCursorPositionApi(HANDLE hConsoleOutput, COORD cursorPos) {

	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, cursorPos));
}


// Fill Console
void WindowApi::FillConsoleOutputCharacterAApi(HANDLE hConsoleOutput, CHAR chars, DWORD cellsToWrite, COORD firstCell, DWORD & charsWritten) {
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, chars, cellsToWrite, firstCell, &charsWritten));
}

void WindowApi::FillConsoleOutputAttributeApi(HANDLE hConsoleOutput, CHAR chars, DWORD cellsToWrite, COORD firstCell, DWORD & charsWritten) {
	
	THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, chars, cellsToWrite, firstCell, &charsWritten));
}


// Read Input / Output Handles
bool WindowApi::ReadConsoleInputApi(HANDLE hConsoleInput, INPUT_RECORD * inBuffer, DWORD inBufferSize, DWORD & numEvents) {
	return ReadConsoleInput(hConsoleInput, inBuffer, inBufferSize, &numEvents);
}

void WindowApi::ReadConsoleOutputAApi(HANDLE hConsoleOutput, CHAR_INFO * bufferData, COORD csbi, COORD bufferCoord, SMALL_RECT & bufferRect) {
	THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(hConsoleOutput, bufferData, csbi, bufferCoord, &bufferRect));
}


// Write Output Handles
void WindowApi::WriteConsoleOutputAApi(HANDLE hConsoleOutput, CHAR_INFO * bufferData, COORD csbi, COORD bufferCoord, SMALL_RECT & bufferRect) {
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, bufferData, csbi, bufferCoord, &bufferRect));
}