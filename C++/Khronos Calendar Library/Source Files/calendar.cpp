/*
khronos/calendar.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Calendar class implementation.
*/

#include <khronos/def.hpp>
#include <khronos/calendar.hpp>
#include <array>
#include <cassert>
#include <cmath>

namespace khronos {
	namespace civil {
		char const * month_name_long(month_t month) {
			static std::array<char const *, 13> const names = { "", "January", "February", "March", "April", "May", "June", "July", 
				"August", "September", "October", "November", "December" };
			assert(month > 0);
			assert(month < 13);
			return names[month];
		}

		char const * month_name_short(month_t month) {
			static std::array<char const *, 13> const names = { "", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", 
				"OCT", "NOV", "DEC" };
			assert(month > 0);
			assert(month < 13);
			return names[month];
		}

		day_t days_in_month(month_t month, bool isLeapYear) {
			static std::array<std::array<day_t, 13>, 2> daysInMonth = {
				0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
				0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
			};
			return daysInMonth[isLeapYear][month];
		} 

		char const* day_name(day_t day) {
			static std::array<char const*, 7> const names = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", 
				"Sunday" };
			assert(day >= 0);
			assert(day < 7);
			return names[day];
		}
	}; //  namespace civil

	namespace islamic {
		char const * month_name(month_t month) {
			static std::array<char const *, 13> const names = { "", "Muharram", "Safar", "Rabi'al-Awwal",
				"Rabi'ath-Thani", "Jumada I-Ula", "Jumada t-Tania", "Rajab", "Sha'ban", "Ramadan", "Shawwal",
				"Dhu I-Qa'da", "Dhu I-Hijja" };
			assert(month > 0);
			assert(month < 13);
			return names[month];
		}

		day_t days_in_month(month_t month, bool isLeapYear) { return month % 2 != 0 || (month == 12 && isLeapYear) ? 30 : 29; }

		char const* day_name(day_t day) {
			static std::array<char const*, 7> const names = { "al-'ahad", "al-'ithnayn", "alth-thalatha", "al-'arb`a'", "al-khamis", 
				"al-jum`a", "as-sabt" };
			assert(day >= 0);
			assert(day < 7);
			return names[day];
		}
	}; //  namespace islamic

	namespace hebrew {
		char const * month_name(month_t month) {
			static std::array<char const*, 14> const names = { "", "Nisan", "Iyyar", "Sivan", "Tammuz", "Av", "Elul", "Tishri", 
				"Heshvan", "Kislev", "Teveth", "Shevat", "Adar", "Veadar" }; 
			assert(month > 0);
			assert(month < 15);
			return names[month];
		}
	}; //  namespace hebrew

	namespace vulcan {
		char const * month_name(month_t month) {
			static std::array<char const *, 13> const names = { "", "Z'at", "D'ruh", "K'riBrax", "re'T'Khutai", "T'keKhuti", 
				"Khuti", "Ta'Krat", "K'ri'lior", "et'khior", "T'lakht", "T'ke'Tas", "Tasmeen", };
			assert(month > 0);
			assert(month < 15);
			return names[month];
		}
	}; // namespace vulcan

}; // namespace calendar