/*
khronos/time_of_day.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos library 'time-of-day' function implementations.
*/

#include <khronos/timeofday.hpp>

namespace khronos {
	void hms(jd_t tod, hour_t& hour, minute_t& minutes, second_t& seconds) {
		long long secondsInDay = static_cast<long long>(floor(tod * SECONDS_PER_DAY + 0.5));
		hour = static_cast<hour_t>(secondsInDay / (60 * 60));
		minutes = static_cast<minute_t>((secondsInDay / 60) % 60);
		seconds = static_cast<second_t>(secondsInDay % 60);
	}
}