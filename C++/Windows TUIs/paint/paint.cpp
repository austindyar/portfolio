/*!	\file		paint.cpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#include "paint.hpp"

// Application
bool done = false;

BOOL CtrlHandler(DWORD ctrlType) {
	switch (ctrlType) {
	case CTRL_C_EVENT:
		done = true;
		return TRUE;
	}

	return FALSE;
}

////////////////////////////////
//			Paint			  //
////////////////////////////////
Paint::Paint() {
	window = Window(CtrlHandler);

	windowWidth = 40;
	windowHeight = 40;
}

int Paint::execute() {
	window.StoreState();
	
	window.ResizeWindow(windowWidth, windowHeight);
	window.SetTitle("Paint");
	window.HideCursor();
	window.SetBackground(White, windowWidth, windowHeight);

	window.UseColors({Red, Green, Blue});

	window.AddCommand(Key::RMouseClick, std::shared_ptr<Command>(new RMouseClickCommand(window)));
	window.AddCommand(Key::LMouseClick, std::shared_ptr<Command>(new LMouseClickCommand(window)));
	window.AddCommand(Key::C, std::shared_ptr<Command>(new CButtonCommand(window, *this)));

	while (!done) {
		window.ProcessInput();
	}

	window.RestoreState();
	return EXIT_SUCCESS;
}

Paint::~Paint() {
	window.RestoreState();
}

////////////////////////////////
//	  LMouseClickCommand	  //
////////////////////////////////
void LMouseClickCommand::execute() {
	window.FillMouseLocation();
}

////////////////////////////////
//	  RMouseClickCommand	  //Bu
////////////////////////////////
void RMouseClickCommand::execute() { 
	window.CycleColors(); 
}

////////////////////////////////
//		 CButtonCommand 	  //
////////////////////////////////
void CButtonCommand::execute() {
	window.SetBackground(White, paint.windowWidth, paint.windowHeight);
}