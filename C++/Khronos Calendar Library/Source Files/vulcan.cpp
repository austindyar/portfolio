/*
khronos/vulcan.cpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Vulcan calendar class implementation.
*/

#include <khronos/vulcan_calendar.hpp>
#include <algorithm>
#include <cassert>
#include <sstream>
#include <ctime>
#include <iomanip>

namespace khronos {
	/**	Vulcan default constructor.  Initialize to the current local time. */
	Vulcan::Vulcan() { 
		Jd jd; 
		jd_to_vulcan(jd.jd(), year_, month_, day_, hour_, minute_, second_); 
	}

	// Julian 6 arg constructor
	Vulcan::Vulcan(year_t year, month_t month, day_t day, hour_t hour, minute_t min, second_t sec) {
		year_ = year;
		month_ = month;
		day_ = day;
		hour_ = hour;
		minute_ = min;
		second_ = sec;
	}

	/** Print the Vulcan Date as a string. */
	std::string Vulcan::to_string() const {
		std::ostringstream oss;
		oss << vulcan_month_name(month_) << ' ' << (unsigned)day_ << ", " << year_ << " " << hour_ << ":" << std::setfill('0') << std::setw(2) << minute_ << ":" << std::setfill('0') << std::setw(2) << second_;
		return oss.str();
	}

	void vulcan_hms(jd_t tod, hour_t& hour, minute_t& minutes, second_t& seconds) {
		long long secondsInAVulcanDay = static_cast<long long>(floor(tod * VULCAN_SECONDS_PER_DAY * (252/266.4)+ 0.5));
		hour = static_cast<hour_t>(secondsInAVulcanDay / (54 * 54));
		minutes = static_cast<minute_t>((secondsInAVulcanDay / 54) % 54);
		seconds = static_cast<second_t>(secondsInAVulcanDay % 54);
	}

	jd_t vulcan_to_jd(year_t year, month_t month, day_t day) {
		jd_t d = day - 1;
		jd_t m = (month - 1) * 21;
		jd_t y = static_cast<jd_t>((year - 1) * 252);
		jd_t jd = (d+m+y)* 266.4 / 252.0;
		return jd + VULCAN_EPOCH;
	}

	jd_t vulcan_to_jd(year_t year, month_t month, day_t day, hour_t hour, minute_t minute, second_t seconds) {
		jd_t jdn = vulcan_to_jd(year, month, day);
		double timeOfDay = vulcan_tod(hour, minute, seconds);
		return jdn + timeOfDay;
	}

	void jd_to_vulcan(jd_t jdn, year_t& year, month_t& month, day_t& day) {
		jdn = jdn - VULCAN_EPOCH;
		year = static_cast<year_t>(ceil(floor(jdn) / 266.4));
		jdn -= year * 266.4;

		jdn < 0 ? jdn = -jdn : jdn = jdn;

		double tempM = (jdn / 22.2);
		if (year < 0) { tempM = 13 - tempM; month = static_cast<month_t>(tempM); }

		else { ++tempM; month = static_cast<month_t>(ceil(tempM)); }

		double tempD = abs(tempM - month) * 21;
		day = static_cast<day_t>(ceil(tempD));

		year == 0 ? year = 1 : year = year;
		day == 0 ? day = 1 : day = day;
	}

	void jd_to_vulcan(jd_t jd, year_t& year, month_t& month, day_t& day, hour_t& hour, minute_t& minute, second_t& second) {
		jd_to_vulcan(jd, year, month, day);
		jd_t tod = vulcan_to_jd(year, month, day);
		tod = jd - tod;
		vulcan_hms(tod, hour, minute, second);
	}
}