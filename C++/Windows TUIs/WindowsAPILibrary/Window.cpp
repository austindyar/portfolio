/*!	\file		Window.cpp
	\author		Austin Dyar
	\date		March 4, 2019
*/

#include "Window.hpp"
Window::Window(BOOL(*handler)(DWORD)) {
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	inBuffer = std::vector<INPUT_RECORD>(128);
	availableColors = std::map<Color, CHAR>{
		{Red, BACKGROUND_RED },
		{Green, BACKGROUND_GREEN },
		{Blue, BACKGROUND_BLUE },
		{Yellow, BACKGROUND_RED | BACKGROUND_GREEN },
		{Black, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE },
		{White, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE},
	};

	asciiToKey = std::map<int, Key>{
		{99, C},
		{114, R}
	};

	// Configure the console mode
	DWORD oldConsoleMode;
	GetConsoleMode(hConsoleInput, &oldConsoleMode);

	DWORD consoleMode = ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT | ENABLE_MOUSE_INPUT;
	consoleMode |= ENABLE_EXTENDED_FLAGS;	// Stop windows from taking over the mouse.

	if (!SetConsoleMode(hConsoleInput, consoleMode)) {
		std::cerr << "\nERROR: could not set console mode.\n";
	}

	// Install a control handler to trap ^C
	if (SetConsoleCtrlHandler((PHANDLER_ROUTINE) handler, TRUE)) {
		std::cout << "Terminate program with Ctrl-C\n";
	} else {
		std::cerr << "ERROR: failed to install control handler." << std::endl;
	}
}

// Window
void Window::StoreState() {
	// Save the title
	originalTitle = std::vector<char>(64 * 1024);
	originalTitle.resize(size_t(thisWindowApi.GetConsoleTitleAApi(originalTitle.data(), (DWORD) originalTitle.size())));
	originalTitle.shrink_to_fit();

	// Get the old window/buffer size
	thisWindowApi.GetConsoleScreenBufferInfoApi(hConsoleOutput, originalCSBI);

	// Save the desktop
	originalBuffer.resize(size_t(originalCSBI.dwSize.X)*originalCSBI.dwSize.Y);
	originalBufferCoord = COORD{ 0 };
	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = originalCSBI.dwSize.X - 1;
	bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
	thisWindowApi.ReadConsoleOutputAApi(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, bufferRect);

	// Save the cursor
	thisWindowApi.GetConsoleCursorInfoApi(hConsoleOutput, originalCCI);

}

void Window::ResizeWindow(size_t width, size_t height) {
	SMALL_RECT sr{ 0 };
	thisWindowApi.SetConsoleWindowInfoApi(hConsoleOutput, sr);

	COORD bufferSize;
	bufferSize.X = (SHORT) width;
	bufferSize.Y = (SHORT) height;
	thisWindowApi.SetConsoleScreenBufferSizeApi(hConsoleOutput, bufferSize);

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	thisWindowApi.GetConsoleScreenBufferInfoApi(hConsoleOutput, csbi);

	sr.Top = sr.Left = 0;
	width = std::min((SHORT) width, csbi.dwMaximumWindowSize.X);
	height = std::min((SHORT) height, csbi.dwMaximumWindowSize.Y);
	sr.Right = (SHORT) width - 1;
	sr.Bottom = (SHORT) height - 1;

	thisWindowApi.SetConsoleWindowInfoApi(hConsoleOutput, sr);
}

void Window::SetTitle(std::string title) {
	thisWindowApi.SetTitleApi(&title[0u]);
}

void Window::RestoreState() {
	// Restore the original settings/size
	SMALL_RECT sr{ 0 };
	thisWindowApi.SetConsoleWindowInfoApi(hConsoleOutput, sr);
	thisWindowApi.SetConsoleScreenBufferSizeApi(hConsoleOutput, originalCSBI.dwSize);
	thisWindowApi.SetConsoleWindowInfoApi(hConsoleOutput, originalCSBI.srWindow);
	
	// Restore the desktop contents
	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = originalCSBI.dwSize.X - 1;
	bufferRect.Bottom = originalCSBI.dwSize.Y - 1;
	thisWindowApi.WriteConsoleOutputAApi(hConsoleOutput, originalBuffer.data(), originalCSBI.dwSize, originalBufferCoord, bufferRect);

	// Restore Title
	thisWindowApi.SetTitleApi(originalTitle.data());

	// Restore the cursor
	thisWindowApi.SetConsoleCursorInfoApi(hConsoleOutput, originalCCI);
	thisWindowApi.SetConsoleCursorPositionApi(hConsoleOutput, originalCSBI.dwCursorPosition);
}


// Input Key Events
void Window::MouseEventProc(MOUSE_EVENT_RECORD const& mer) {
	switch (mer.dwEventFlags) {
	case 0: {
		auto mask = mer.dwButtonState;
		if (mask&FROM_LEFT_1ST_BUTTON_PRESSED) {
			if (availableCommands.find(LMouseClick) != availableCommands.end()) {
				mouseEvent = mer;
				availableCommands.find(LMouseClick)->second->execute();
			}
		}

		if (mask&RIGHTMOST_BUTTON_PRESSED) {
			if (availableCommands.find(RMouseClick) != availableCommands.end()) {
				mouseEvent = mer;
				availableCommands.find(RMouseClick)->second->execute();
			}
		}
		break;
	}
	}
}

void Window::KeyEventProc(KEY_EVENT_RECORD const & ker) {
	if (asciiToKey.find(ker.uChar.AsciiChar) != asciiToKey.end()) {
		if (availableCommands.find(asciiToKey.find(ker.uChar.AsciiChar)->second) != availableCommands.end()) {
			availableCommands.find(asciiToKey.find(ker.uChar.AsciiChar)->second)->second->execute();
		}
	}
}


// Background
void Window::SetBackground(std::vector<CHAR_INFO> background) {
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	thisWindowApi.GetConsoleScreenBufferInfoApi(hConsoleOutput, csbi);

	background.resize(size_t(csbi.dwSize.X)*csbi.dwSize.Y);
	COORD backgroundBufferCoord = COORD{ 0 };
	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = csbi.dwSize.X - 1;
	bufferRect.Bottom = csbi.dwSize.Y - 1;
	thisWindowApi.WriteConsoleOutputAApi(hConsoleOutput, background.data(), csbi.dwSize, backgroundBufferCoord, bufferRect);
}

void Window::SetBackground(Color color, size_t width, size_t height) {
	CHAR_INFO charInfo;
	charInfo.Attributes = GetColorDefinition(color);
	charInfo.Char.AsciiChar = ' ';
	std::vector<CHAR_INFO> background = std::vector<CHAR_INFO>(width * height, charInfo);

	SetBackground(background);
}

std::vector<CHAR_INFO> Window::GetBackground() {
	std::vector<CHAR_INFO> background;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	thisWindowApi.GetConsoleScreenBufferInfoApi(hConsoleOutput, csbi);

	background.resize(size_t(csbi.dwSize.X)*csbi.dwSize.Y);
	COORD backgroundBufferCoord = COORD{ 0 };
	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = csbi.dwSize.X - 1;
	bufferRect.Bottom = csbi.dwSize.Y - 1;
	thisWindowApi.ReadConsoleOutputAApi(hConsoleOutput, background.data(), csbi.dwSize, backgroundBufferCoord, bufferRect);
	return background;
}


// Color
void Window::UseColors(std::vector<Color> colors) {
	for (size_t index = 0; index < colors.size(); index++) {
		if (availableColors.find(colors[index]) != availableColors.end()) {
			usedColors.push_back(availableColors.find(colors[index])->second);
		}
	}
	currentColor = 0;
}

void Window::CycleColors() {
	if (currentColor == usedColors.size() - 1) {
		currentColor = 0;
	} else {
		++currentColor;
	}
}

WORD Window::GetColorDefinition(Color color) {
	return availableColors.at(color);
}

CHAR Window::GetCurrentColor() {
	return usedColors[currentColor];
}


// Command
void Window::AddCommand(Key key, std::shared_ptr<Command> const& command) {
	if (command != nullptr) {
		availableCommands[key] = command;
	} else {
		std::cerr << "Provide a valid command\n";
		return;
	}
}

// Mouse
COORD Window::GetMouseLocation() {
	return mouseEvent.dwMousePosition;
}

void Window::FillMouseLocation() {
	DWORD charsWritten = 0;
	thisWindowApi.FillConsoleOutputAttributeApi(hConsoleOutput, usedColors[currentColor], 1, mouseEvent.dwMousePosition, charsWritten);
}

// Cursor
void Window::HideCursor() {
	CONSOLE_CURSOR_INFO newCCI = originalCCI;
	newCCI.bVisible = FALSE;
	thisWindowApi.SetConsoleCursorInfoApi(hConsoleOutput, newCCI);
}


// Input
void Window::ProcessInput() {
	DWORD numEvents;

	if (!thisWindowApi.ReadConsoleInputApi(hConsoleInput, inBuffer.data(), (DWORD) inBuffer.size(), numEvents)) {
		std::cerr << "Failed to read console input\n";
		return;
	}

	for (size_t iEvent = 0; iEvent < numEvents; ++iEvent) {
		switch (inBuffer[iEvent].EventType) {
		case MOUSE_EVENT:
			MouseEventProc(inBuffer[iEvent].Event.MouseEvent);
			break;
		case KEY_EVENT:
			KeyEventProc(inBuffer[iEvent].Event.KeyEvent);
		}
	}
}