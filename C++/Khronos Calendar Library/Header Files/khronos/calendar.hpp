#pragma once

/*
khronos/calendar.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-11-24

Khronos calendar library declarations.
*/

#include <khronos/def.hpp>
#include <khronos/utility.hpp>
#include <khronos/timeofday.hpp>
#include <limits>

namespace khronos {
	// Civil 
	// =============================================
	enum civil_month_codes_long { January = 1, February, March, April, May, June, July, August, September, October, November, December };
	enum civil_month_codes_short { JAN = January, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC };
	enum civil_weekday_codes_long { Monday = 0, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };
	enum civil_weekday_codes_short { MON = Monday, TUE, WED, THU, FRI, SAT, SUN };

	// Islamic
	// =============================================
	enum islamic_month_codes { Muharram = 1, Safar, RabialAwwal, RabiathThani, JumadaalAwwal, JumadatTania, Rajab, Shaban, Ramadan, Shawwal, DhulQadah, DhulHijja};

	// Hebrew
	// =============================================
	enum hebrew_month_codes { Nisan = 1, Iyyar, Sivan, Tammuz, Av, Elul, Tishri, Heshvan, Kislev, Teveth, Shevat, Adar, Veadar };

	// Vulcan
	// =============================================
	enum vulcan_month_codes { Zat = 1, Druh, KriBrax, reTKhutai, TkeKhuti, Khuti, TaKrat, Krilior, etkhior, Tlakht, TkeTas, Tasmeen, };

	// FUNCTIONS
	// =============================================
	namespace civil {
		char const* month_name_long(month_t month);
		char const* month_name_short(month_t month);

		char const* day_name(day_t day);
		inline day_t day_of_week(jd_t jd) { return static_cast<day_t>(utility::mod(jd + 0.5, 7)); }
		day_t days_in_month(month_t month, bool isLeapYear);
	}

	namespace islamic {
		char const* month_name(month_t month);
		day_t days_in_month(month_t month, bool isLeapYear);

		char const* day_name(day_t day);
		inline day_t day_of_week(jd_t jd) { return static_cast<day_t>(utility::mod(jd + 0.5, 7)); }
	}

	namespace hebrew {
		char const * month_name(month_t month);
	}
	
	namespace vulcan {
		char const * month_name(month_t month);
	}

	// ALTERNATE QUERY
	// =============================================
	template <class Calendar> inline year_t year(Calendar const& date) { return date.year(); }
	template <class Calendar> inline month_t month(Calendar const& date) { return date.month(); }
	template <class Calendar> inline day_t day(Calendar const& date) { return date.day(); }

	template <class Calendar> inline hour_t hour(Calendar const& date) { return date.hour(); }
	template <class Calendar> inline minute_t minute(Calendar const& date) { return date.minute(); }
	template <class Calendar> inline second_t second(Calendar const& date) { return date.second(); }
};