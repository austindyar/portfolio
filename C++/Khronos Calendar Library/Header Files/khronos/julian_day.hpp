#pragma once

/*
khronos/julian_day.hpp
(c) Austin Dyar
Created: 2017-09-22
Last Updated: 2017-12-3

Julian day number header.
*/

#include <khronos/calendar.hpp>

namespace khronos {
	// FUNCTIONS
	// =============================================

	/** Convert a Julian date to a Julian day number. */
	inline jd_t jd_to_jdn(jd_t jd) { return static_cast<jd_t>(floor(jd + 0.5)); }

	// CLASSES
	// --------------------------------------------------------------------------------------

	/**	Julian Date (JD) class. */
	class Jd {
		jd_t jd_;
	public:
		Jd();
		Jd::Jd(std::string input);

		/**	Constructor from real number. Also implicit cast.*/
		Jd(jd_t jd) : jd_(jd) {}

		/**	Get the number of days. */
		jd_t jd() const { return jd_; }

		std::string to_string() const;
	};

	/** Stream insertion operator. */
	inline std::ostream& operator << (std::ostream& os, Jd const& jd) { return os << jd.to_string(); }

	/**	Day of the week. */
	inline day_t day_of_week(Jd const& jd) { return civil::day_of_week(jd.jd()); }

	// OPERATORS
	// =============================================

	/** Jd relational operators. */
	inline bool operator == (Jd const& lhs, Jd const& rhs) { return lhs.jd() == rhs.jd(); }
	inline bool operator != (Jd const& lhs, Jd const& rhs) { return lhs.jd() != rhs.jd(); }
	inline bool operator <= (Jd const& lhs, Jd const& rhs) { return lhs.jd() <= rhs.jd(); }
	inline bool operator >= (Jd const& lhs, Jd const& rhs) { return lhs.jd() >= rhs.jd(); }
	inline bool operator < (Jd const& lhs, Jd const& rhs) { return lhs.jd() < rhs.jd(); }
	inline bool operator > (Jd const& lhs, Jd const& rhs) { return lhs.jd() > rhs.jd(); }

	/**Jd difference operator. */
	inline jd_t operator - (Jd const& lhs, Jd const& rhs) { return lhs.jd() - rhs.jd(); }

	/** Day addition/subtraction. */
	namespace detail {
		struct packaged_day {
			day_t nDays_;
			packaged_day(day_t d) : nDays_(d) {}
		};
	};

	inline detail::packaged_day days(day_t d) { return detail::packaged_day(d); }
	inline Jd operator + (Jd const& jd, detail::packaged_day days) { return Jd(jd.jd() + days.nDays_); }
	inline Jd operator - (Jd const& jd, detail::packaged_day days) { return Jd(jd.jd() - days.nDays_); }

	/** Week addition/subtraction. */
	namespace detail {
		struct packaged_week {
			day_t nWeeks_;
			packaged_week(day_t w) : nWeeks_(w) {}
		};
	};

	inline detail::packaged_week weeks(day_t w) { return detail::packaged_week(w); }
	inline Jd operator + (Jd const& jd, detail::packaged_week weeks) { return Jd(jd.jd() + 7.0 * weeks.nWeeks_); }
	inline Jd operator - (Jd const& jd, detail::packaged_week weeks) { return Jd(jd.jd() - 7.0 * weeks.nWeeks_); }

	//** Month addition/subtraction. */
	namespace detail {
		struct packaged_month_integer {
			month_t nMonths_;
			packaged_month_integer(month_t m) :nMonths_(m) {}
		};
		struct packaged_month_real {
			month_t nMonths_;
			packaged_month_real(month_t m) :nMonths_(m) {}
		};
	}

	inline detail::packaged_month_real months(double m) { return detail::packaged_month_real(static_cast<int>(m)); }
	inline detail::packaged_month_real months(float m) { return detail::packaged_month_real(static_cast<int>(m)); }

	template <typename T> inline detail::packaged_month_integer months(T m) { return detail::packaged_month_integer(month_t(m)); }

	//** Year addition/subtraction. */
	namespace detail {
		struct packaged_year_integer {
			year_t nYears_;
			packaged_year_integer(year_t y) : nYears_(y) {}
		};
		struct packaged_year_real {
			double nYears_;
			packaged_year_real(double y) : nYears_(y) {}
		};
	};

	template <typename T> inline detail::packaged_year_integer years(T y) { return detail::packaged_year_integer(month_t(y)); }
	inline detail::packaged_year_real years(double y) { return detail::packaged_year_real(y); }
	inline detail::packaged_year_real years(float y) { return detail::packaged_year_real(y); }

	inline Jd operator + (Jd const& jd, detail::packaged_year_integer years) { return Jd(jd.jd() + 7.0 * years.nYears_); }
	inline Jd operator - (Jd const& jd, detail::packaged_year_integer years) { return Jd(jd.jd() - 7.0 * years.nYears_); }
	inline Jd operator + (Jd const& jd, detail::packaged_year_real years) { return Jd(jd.jd() + years.nYears_ * EARTH_ORBITAL_PERIOD_DAYS); }
	inline Jd operator - (Jd const& jd, detail::packaged_year_real years) { return jd + detail::packaged_year_real(-years.nYears_); }
};
